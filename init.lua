classroom = {}

-- Required MT version
assert(minetest.features.formspec_version_element, "Minetest 5.1 or later is required")

-- Internationalisaton
classroom.S = minetest.get_translator("classroom")
classroom.FS = function(...)
	return minetest.formspec_escape(classroom.S(...))
end

-- Source files
dofile(minetest.get_modpath("classroom") .. "/api.lua")
dofile(minetest.get_modpath("classroom") .. "/gui_dash.lua")
dofile(minetest.get_modpath("classroom") .. "/gui_group.lua")
dofile(minetest.get_modpath("classroom") .. "/freeze.lua")
dofile(minetest.get_modpath("classroom") .. "/actions.lua")

-- Privileges
minetest.register_privilege("teacher", {
	give_to_singleplayer = false
})

-- Hooks needed to make api.lua testable
classroom.get_connected_players = minetest.get_connected_players
classroom.get_player_by_name = minetest.get_player_by_name
classroom.check_player_privs = minetest.check_player_privs

-- Mod storage for persistence
local storage = minetest.get_mod_storage()

classroom.load_from(storage)

function classroom.save()
	classroom.save_to(storage)
end

minetest.register_on_shutdown(classroom.save)
