unused_args = false
allow_defined_top = true

globals = {
	"classroom",
}

read_globals = {
	"minetest",
	"sfinv",
	"vector",
	"unified_inventory",
	table={fields={"copy"}},
}
