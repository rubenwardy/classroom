#!/bin/bash

find_minetest() {
	MT_DIR=$(pwd)
	while [[ $(basename "$MT_DIR") != "minetest" ]]
	do
		MT_DIR=$(dirname "$MT_DIR")
	done
	MT=${MT_DIR}/bin/minetest
	echo "Found minetest at: $MT_DIR"
}

set_conf() {
	# Make readonly config
	conf=/tmp/$1.conf
	chmod 0644 $conf # incase it already exists
	cp utils/$1.conf /tmp/$1.conf
	chmod 0444 $conf
}

start_server() {
	echo "Starting server"
	$MT --server --port 30000 --world "${MT_DIR}/worlds/edu" --name "teacher1" --config "$conf" &
}

start_client() {
	echo "Starting client $1"
	$MT --address "127.0.0.1" --port 30000 --name "$1" --password "" --go &
}

start_headless_client() {
	echo "Starting headless client $1"
	xvfb-run $MT --address "127.0.0.1" --name $1 --password "" --config "$conf" --go &
}


find_minetest

set_conf server
start_server

set_conf client
start_headless_client student1
start_headless_client student2
start_headless_client student3
start_headless_client student4
start_headless_client student5
start_headless_client student6

start_client teacher1


read -n 1 -s -r -p "Press any key to continue"

killall minetest
