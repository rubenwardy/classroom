# Classroom Manager

An easy way to manage students in-game, with support for bulk actions and macros.

An action is something that can be performed on a selection of students.
You can use actions to move players, grant/revoke privs, and more.
A list of built-in actions can be found below, however, you can
add more actions using the API for this mod.

This mod requires Minetest 5.1 or later.

Author: rubenwardy<br>
License: MIT

## Usage

First, make sure that you grant the `teacher` priv to any teachers like so:

	/grant username teacher

This allows them to access the classroom control panel, and exempts them from
being included in actions.

Open the Inventory and switch to the classroom tab.
Only sfinv is supported currently.

* The set of students that an action works on depends on the toggle bar at the top.
	* **All:** apply the action to all students, but not teachers.
	* **Group:** apply the action to all students in the currently selected group.
	* **Selected:** apply the action only to the student that is currently selected.
* You can changed the selected group using the tab header to the left.
* You can manage groups using the New/Edit Group buttons in the
  top-right.
	* Use `>` to add the selected player to the group, `<` to remove.

### Actions

| Name        | Behaviour                                                          |
|-------------|--------------------------------------------------------------------|
| Bring       | Teleports users to your position                                   |
| Look        | Rotates the users to look at the teacher                           |
| Mute/Unmute | Grants or revokes the ability to use chat                          |
| Fly/No fly  | Grants or revokes the ability to fly (key: K)                      |
| Kick        | Remove from the server                                             |
| Ban         | Permanently exclude from the server                                |
| Audience    | Move to the crosshair location, spread out and facing you          |

## API

**WARNING:** This API is unstable, and breaking changes may be introduced before 1.0 is released.

### Selectors

A selector is a string which represents a selection of students.

* `*` - All students
* `group:<name>` - All students in group `<name>`
* `user:<name>` - The user `<name>`

### Actions

```lua
classroom.register_action("mymod:bring", {
	-- Button text
	title = "Bring",

	-- Button tooltip
	description = "Teleport players to your location",

	-- Whether this should be restricted to online players
	--  TODO: false isn't supported yet
	online_required = true,

	-- Function to perform the action
	func = function(runner, names)
		local pos = runner:get_pos()
		for i=1, #names do
			local player = minetest.get_player_by_name(names[i])
			player:set_pos(pos)
		end
	end,

	-- Custom keys are allowed
})
```

### Group

```lua
{
	-- Human-readable group name
	name = name,

	-- List of student names
	students = {},

	-- Custom keys are allowed
}
```

### Methods

* `classroom.get_students()`
	* Get online students
	* Returns list of player names
* `classroom.get_group_students(name)`
	* Get students in group, some students may be offline
	* Returns list of player names
* `classroom.get_students_except(excluded_students)`
	* Get online students not in the list `excluded_students`
	* Returns list of player names
* `classroom.get_all_groups()`
	* Returns table of `groupname -> group`
* `classroom.get_group(name)`
	* Returns group
* `classroom.create_group(name)`
	* Creates a group
	* Returns the newly created group or nil if the name already exists
* `classroom.add_student_to_group(group_name, student_name)`
* `classroom.remove_student_from_group(group_name, student_name)`
* `classroom.register_action(name, action_def)`
* `classroom.get_actions()`
	* Returns list of action defs
* `classroom.get_students_by_selector(selector)`
	* Gets online students using a selector.
	* Returns list of player names
