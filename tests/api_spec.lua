local function fake_player(name)
	return {
		get_player_name = function()
			return name
		end,
	}
end

local function recreate_classroom()
	_G.classroom = {
		S = string.format,
		get_connected_players = function()
			return {
				fake_player("user1"),
				fake_player("user2"),
				fake_player("user3"),
				fake_player("teacher"),
			}
		end,
		check_player_privs = function(player, privs)
			if not privs.teacher then
				return false
			end

			if type(player) == "string" then
				return player == "teacher"
			else
				return player:get_player_name() == "teacher"
			end
		end,
		get_player_by_name = function(name)
			if name ~= "fakeuser" then
				return fake_player(name)
			end
		end
	}

	dofile("api.lua")
end

describe("classroom", function()
	it("has students", function()
		recreate_classroom()

		local students = classroom.get_students()
		assert.equal(3, #students)
		assert.equal("user1", students[1])
		assert.equal("user2", students[2])
		assert.equal("user3", students[3])
	end)

	it("has except students", function()
		recreate_classroom()

		local students = classroom.get_students_except({ "user2" })
		assert.equal(2, #students)
		assert.equal("user1", students[1])
		assert.equal("user3", students[2])

		students = classroom.get_students_except({ "user3" })
		assert.equal(2, #students)
		assert.equal("user1", students[1])
		assert.equal("user2", students[2])

		students = classroom.get_students_except({ "user3", "user2", "user1" })
		assert.equal(0, #students)
	end)
end)

describe("groups", function()
	it("has students", function()
		recreate_classroom()

		assert.is_nil(classroom.get_group("test"))
		classroom.create_group("test")
		assert.is_not_nil(classroom.get_group("test"))
		assert.equals(0, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user1")
		assert.equals(1, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user1")
		assert.equals(1, #classroom.get_group("test").students)
		classroom.remove_student_from_group("test", "user2")
		assert.equals(1, #classroom.get_group("test").students)
		classroom.remove_student_from_group("test", "user1")
		assert.equals(0, #classroom.get_group("test").students)
	end)
end)

describe("selectors", function()
	it("all", function()
		recreate_classroom()

		local students = classroom.get_students_by_selector("*")
		assert.equal(3, #students)
		assert.equal("user1", students[1])
		assert.equal("user2", students[2])
		assert.equal("user3", students[3])
	end)

	it("groups", function()
		recreate_classroom()

		local students = classroom.get_students_by_selector("group:none")
		assert.is_nil(students)

		assert.is_nil(classroom.get_group("test"))
		classroom.create_group("test")
		assert.is_not_nil(classroom.get_group("test"))
		assert.equals(0, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user1")
		assert.equals(1, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user2")
		assert.equals(2, #classroom.get_group("test").students)

		students = classroom.get_students_by_selector("group:test")
		assert.equals(2, #students)
		assert.equals("user1", students[1])
		assert.equals("user2", students[2])
	end)

	it("students", function()
		recreate_classroom()

		local students = classroom.get_students_by_selector("user:user1")
		assert.equals(1, #students)
		assert.equals("user1", students[1])

		assert.is_nil(classroom.get_group("test"))
		classroom.create_group("test")
		assert.is_not_nil(classroom.get_group("test"))
		assert.equals(0, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user1")
		assert.equals(1, #classroom.get_group("test").students)
		classroom.add_student_to_group("test", "user2")
		assert.equals(2, #classroom.get_group("test").students)

		students = classroom.get_students_by_selector("user:user1")
		assert.equals(1, #students)
		assert.equals("user1", students[1])
	end)
end)
