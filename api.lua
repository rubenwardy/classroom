local _groups = {}
local _action_by_name = {}
local _actions = {}

-- Load data from MetaDataRef
function classroom.load_from(meta)
	local groups_str = meta:get("groups")
	if not groups_str then
		return
	end

	_groups = minetest.deserialize(groups_str)
end

-- Save data to MetaDataRef
function classroom.save_to(meta)
	meta:set_string("groups", minetest.serialize(_groups))
end

function classroom.save()
	-- Overridden in init.lua
end

function classroom.get_students()
	local students = {}
	for _, player in pairs(classroom.get_connected_players()) do
		if not classroom.check_player_privs(player, { teacher = true }) then
			students[#students + 1] = player:get_player_name()
		end
	end

	return students
end

function classroom.get_group_students(name)
	local group = classroom.get_group(name)
	if not group then
		return nil
	end

	local students = {}
	for _, student in pairs(group.students) do
		if classroom.get_player_by_name(student) then
			students[#students + 1] = student
		end
	end

	return students
end

function classroom.get_students_except(students)
	local student_by_name = {}
	for _, name in pairs(students) do
		student_by_name[name] = true
	end

	local retval = {}
	for _, player in pairs(classroom.get_connected_players()) do
		if not student_by_name[player:get_player_name()] and
				not classroom.check_player_privs(player, { teacher = true }) then
			retval[#retval + 1] = player:get_player_name()
		end
	end

	return retval
end

function classroom.get_all_groups()
	return _groups
end

function classroom.get_group(name)
	return _groups[name]
end

function classroom.create_group(name)
	if _groups[name] or #name == 0 then
		return nil
	end

	local group = {
		name = name,
		students = {},
	}

	_groups[name] = group

	classroom.save()

	return group
end

function classroom.add_student_to_group(name, student)
	local group = classroom.get_group(name)
	if group then
		for i=1, #group.students do
			if group.students[i] == student then
				return
			end
		end

		group.students[#group.students + 1] = student

		classroom.save()
	end
end

function classroom.remove_student_from_group(name, student)
	local group = classroom.get_group(name)
	if group then
		for i=1, #group.students do
			if group.students[i] == student then
				table.remove(group.students, i)

				classroom.save()
			end
		end
	end
end

function classroom.register_action(name, def)
	def.name = name
	_action_by_name[name] = def
	table.insert(_actions, def)
end

function classroom.get_actions()
	return _actions
end

function classroom.get_students_by_selector(selector)
	if selector == "*" then
		return classroom.get_students()
	elseif selector:sub(1, 6) == "group:" then
		return classroom.get_group_students(selector:sub(7))
	elseif selector:sub(1, 5) == "user:" then
		local pname = selector:sub(6)
		if classroom.get_player_by_name(pname) then
			return { pname }
		else
			return {}
		end
	else
		return {}
	end
end

function classroom.run_action(aname, runner, selector, params)
	local action   = _action_by_name[aname]
	local students = classroom.get_students_by_selector(selector)
	if #students > 0 then
		action.func(runner, students)
	end
end
